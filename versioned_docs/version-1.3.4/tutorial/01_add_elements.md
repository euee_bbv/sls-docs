---
sidebar_position: 2
---

# Добавляем элементы

## Анализируем элементы экранов

Рассмотрев подробнее макет приложения, мы увидим, что у нас есть несколько элементов:

- изображение (рядом с текстом LOGO)
- текст (LOGO, значение в процентах, и надпись на кнопке)
- дуга
- слайдер
- панель на верху

Так же выделим основные цвета, которые будут использованы в приложении:

- `#000000` - цвет верхней панели, фон слайдера и дуги, цвет текста на кнопке
- `#27272A` - цвет заднего фона экранов
- `#FF1744` - акцентный цвет на втором экране
- `#FFC400` - акцентный цвет на первом экране
- `#FFFFFF` - основной цвет текста

Так же выделим общие элементы на обоих экранах: задний фон, верхняя панель, кнопка. На экранах изменяются только акцентные цвета и элементы управления.

## Стартовый экран

Начнём с наполнения первого экрана.

Для начала зададим цвет заднего фона экрана.
В панели **Screens** выделяем наш экран, переходим в панель **Inspector**. Там во вкладке **Screen** зададим в поле **Name** имя нашему экрану - **home**.

Далее во вкладке **_Style Settings_** -> **_Style Main_** -> **_Background_** поставим флажок в поле **_Bg Color_** и введем значение `#27272A`. В поле просмотра увидим, что экарн поменял цвет на необходимый нам.

### Добавляем элементы

Создадим верхнюю панель. По макету она занимает 480px (100%) ширины экрана и 60px по высоте. Так же в данной панеле размещается изображение (меняет цвет при смене экранов) и текст.

Реализовать данную панель можно несколькими способами:

- добавить изображение панели и разместить на экране
- добавить виджет **Container** и стилизовать его в соответствии с макетом
- добавить виджет **Panel** и стилизовать его в соответствии с макетом

Выбираем способ с виджетом **Panel**. В панели **Screens** выделяем наш экран, переходим в панель **Hierarchy**. В панели **_Widgets_ ->_Basic_** выбираем **Panel**.

В левом верхнем углу будет создан видже с именем **Panel1** размерами 100х50 пикселей с координатами X: 0, Y: 0. Так как мы знаем, что панель занимает 480 пикселей по ширине и 60 по высоте, то мы можем такие размеры и задать. Либо же вместо 480 пикселей указать 100 % или 1 Content (данные единицы необходимы для построения сеток, по аналогии с CSS Flex / Grid в web разработке). Так же можно поступить и с высотой (получится 12.5 %, однако применять дробные значения нельзя).

Установив размеры, зададим цвет панели (по аналогии с цветом заднего фона экрана):

- **_Style Settings_** -> **_Style Main_** -> **_Background_** -> **_Bg Color_** укажем `000000`

Далее разместим в нашей панели текст и логотип.

Так как у нас используется стандартный шрифт программы Montserrat, то нам не нужно добавлять шрифты в папку Assets. Так же в программе есть необходимые нам размеры шрифта в 24pt и 48pt.

[**_Как создать | добавить шрифт в проект_**](/dev_flow/panels/font_manager)

Перейдем в панель Assets и оттуда вынесем наш логотип (или создаем виджет **Image** и в качестве параметра Asset укажем наш логотип), назовем его logo. Так же создадим виджет **Label**. назовем его logoDesc.

Оба виджета создались в нулевых координатах, что нас не устаривает. Мы можем воспользоваться возможностью расстановки элементов по сетке, чтобы расставить элементы, либо поставить их по нужным координатам.

Воспользуемся сеткой. Выделим наши элементы в панели **Hierarchy**, и с зажатой кнопкой мыши перетащим их на наш виджет **Panel1**. В нашем виджете слева появится стрелочка, которая покажет, что в данном виджете имеется вложенность элементов. В панели **_Inspector_** -> **_Layout_** -> **_Flex Flow_** выберем **Flex**. Снизу появятся дополнительные поля (самостоятельная работа: для понимания работы сеток советую посмотерть, как ведут себя элементы при выборе разных из свойств).

По макету у нас логотип расположен в координатах X: 25, Y: 15, текст имеет координаты X: 80, Y: 20 (изображение стоит по центру панели с отступом в 25 пикселей, отступ между текстом и изображением то же 25 пикселей).

Исходя из данной информации выставим параметры:

- **_PANEL_** -> **_FLEX FLOW_** -> **_Track_** -> **_Center_**
- **_Style Settings_** -> **_Style Main_** -> **_Paddings_** -> **_Pad Left_** установим значение 25
- **_Style Settings_** -> **_Style Main_** -> **_Paddings_** -> **_Pad Column_** установим значение 25

Так же зададим общий стиль для текста, который находится в данном виджете:

- **_Style Settings_** -> **_Style Main_** -> **_Text_** -> **_Text Color_** установим `FFFFFF`
- **_Style Settings_** -> **_Style Main_** -> **_Text_** -> **_Text Font_** выберем `montserrat 24`
- выделим наш текст и заменим его на тот, что в макете (LOGO)

Добавим кнопку на экран (X: 110, Y: 390, ширина 260px, высота 48px), используем виджет **Button**, назовем **btnArc**. Добавим свойство **Flex**, и свойства **_Main Dir Align_**, **_Cross Dir Align_** и **_Track_** установим **Center**.

Стилизуем в соответствии с макетом:

- **_Style Settings_** -> **_Style Main_** -> **_Background_** -> **_Bg Radius_** установим значение 48 (радус скругления кнопки)
- **_Style Settings_** -> **_Style Main_** -> **_Background_** -> **_Bg Color_** укажем `FFC400`
- **_Style Settings_** -> **_Style Main_** -> **_Text_** -> **_Text Color_** установим `000000`
- **_Style Settings_** -> **_Style Main_** -> **_Text_** -> **_Text Font_** выберем `montserrat 24`
- добавим виджет **Label**, перенесем его в кнопку (по аналогии с панелью), назовем **btnLbl** и текст заменим на `SLIDER`.

Добавим виджет, который будет показывать значение от дуги и слайдера (X: 150, Y: 190, ширина 180px, высота 60px, montserrat 48, цвет `FFFFFF`, параметр **_Text Align_** -> **_Center_**), назовем **lblSld**.

Вернемся к логотипу. Выбираем его, далее:

- во вкладке **_Style Settings_** -> **_State_** выбираем **USER_1**
- **_Style Settings_** -> **_Style (MAIN)_** -> **_Image_** -> **_Image reColor_** задаем `FF1744`, значение **Alpha** устанаваливаем 255
- теперь, если во вкладке **_Image_** -> **_States_** установить флаг User 1, мы увидим, что логотип изменяет цвет на красный

Сделаем все те же операции с кнопкой (самостоятельная работа)

> изменить стиль **_Bg Radius_**, **_Bg Color_**, **_Text Color_** для состояния **USER_1**

---

На данном этапе у нас на 2 экранах имеются одинаковые элементы, и мы можем сделать дубликат экрана, чтобы заново не расставлять все элементы. Перейдем в панель **Screens**, выделим наш экран. Справа появятся 3 иконки, нажмем на 3 (**Duplicate**). Появится дубликат нашего экрана с именем **home1** (имя оставим, можем дать другое).

На втором экране текст кнопки заменим на `ARC`.

Если все сделано верно, то рабочая область должна выглядеть так:

![step_01](/img/0002.jpg)

---
