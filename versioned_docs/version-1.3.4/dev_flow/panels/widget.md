---
sidebar_position: 9
---

# Панель Widgets

Виджеты - основные элементы пользовательского интерфейса.
У каждого виджета есть свои состояния и настройки. Вы можете добавлять собственные стили к компонентам виджетов и настраивать их в разных состояниях.

Дополнительная информация о [**виджетах**](/category/виджеты)
