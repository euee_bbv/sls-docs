---
sidebar_position: 7
---

# Действия и события

Добавляя события, вы можете создавать различные взаимодействия с виджетами, например, менять экран, воспроизводить анимацию нажатием кнопки и т. д.

## Add Event

В нижней части панели [**инспектора**](/dev_flow/panels/inspector) вы можете найти кнопку **Add Event**. Сначала вам следует назвать событие, а затем выбрать триггер для его запуска.

- **Event name** - название события
- **Event Trigger** - триггер для вызова события:
  - **Pressed** - объект был нажат
  - **Clicked** - объект был нажат в течение короткого периода времени, а затем отпущен. Не вызывается при прокрутке
  - **Long pressed** - объект нажат в течение длительного периода времени
  - **Long pressed repeat** - вызов `long_press_time` через каждую `long_press_repeat_time` мс. Не вызывается при прокрутке
  - **Focused** - объект находистя в фокусе
  - **Defocused** - объект не в фокусе
  - **Value changed** - значение объекта было изменено
  - **Ready** - процесс завершен
  - **Cancel** - процесс отменен
  - **Screen loaded** - экран был загружен, вызывается после завершения всех анимаций
  - **Screen unloaded** - экран выгружен, вызывается после завершения всех анимаций
  - **Screen load start** - началась загрузка экрана, которая запускается по истечении задержки смены экрана
  - **Screen unload start** - началась выгрузка экрана, которая запускается сразу при вызове `lv_scr_load/lv_scr_load_anim`
  - **Checked** - отмеченное состояние у виджета
  - **Unchecked** - отмеченное состояние у виджета снято
  - **Gesture** - на виджете обнаруживается жест с выбранным направлением (свайп сенсорного экрана)
  - **Short clicked** - обнаружено короткое нажатие
  - **Key** - в виджет отправляется ключ

## Add Action

Добавив взаимодействие, следует добавить действие, которое необходимо выполнить

- **Action name** - название действия
- **Action type** - тип действия

## Actions

Действия — это те элементы события, которые запускаются при возникновении триггера

### Call function

Используя действие **Call function**, вы можете добавить имя функции, на которую будет ссылаться событие.
"Тело" функции будет создано в файле `ui__events.c` или `ui_events.py` в процессе экспорта.

### Change Screen

С помощью этого действия вы можете переключаться между экранами

- **Screen to** - экран, на который вы хотите перейти
- **Fade mode** - анимация при смене экрана
- **Speed** - скорость смены экрана
- **Delay** - задержка смены экрана

### Increment Arc

Изменение значения **Arc Indicator** виджета **Arc** на заданное значение

- **Target** - виджет целевой дуги
- **Value** - значение увеличения/уменьшения

### Increment Bar

Изменение значения **Bar Indicator** виджета **Bar** на заданное значение

- **Target** - виджет целевой панели
- **Value** - значение увеличения/уменьшения
- **Animate** - анимации изменения значения (вкл/выкл)

### Increment Slider

Изменение значения **Slider Indicator** виджета **Slider** на заданное значение

- **Target** - виджет целевого слайдера
- **Value** - значение увеличения/уменьшения
- **Animate** - анимации изменения значения (вкл/выкл)

### Modify Flag

Изменение состояния флага виджета

- **Object** - целевой объект
- [**Flags**](/dev_flow/panels/inspector#flags) - флаг, который нужно изменить
- **Action** - действие, которое будет использоваться:
  - **Add** - добавить флаг
  - **Remove** - удалить флаг
  - **Toggle** - переключатель (Add | Remove)

### Play Animation

Вы можете воспроизводить анимацию, созданную на панели **Animations**

- **Animation** - выбранная анимация
- **Target** - целевой виджет, для которого вы хотите использовать анимацию
- **Delay** - время задержки анимации

### Set Opacity

Установка непрозрачности выбранного виджета

- **Target** - целевой виджет
- **Value** - значение непрозрачности

### Set Flag

Установка значения состояния флага виджета

- **Object** - целевой объект
- **Flag** - флаг, который будет использоваться
- **Value** - значение состояния флага

### Set Property

Изменение значения свойства виджета

- **Target** - целевой объект
- **Property** - параметр, который необходимо изменить
- **Value** - значение свойства

### Set text value from arc

Отображение значения **Arc Indicator** виджета **Arc** в виджете **Label**

- **Target** - виджет **Label**, в котором отображается значение
- **Source** - текущее значение **Arc Indicator** виджета **Arc**
- **Prefix** - текст перед значением в виджете **Label**
- **Postfix** - текст после значения в виджете **Label**

### Set text value from slider

Отображение значения **Slider Indicator** виджета **Slider** в виджете **Label**

- **Target** - виджет **Label**, в котором отображается значение
- **Source** - текущее значение **Slider Indicator** виджета **Slider**
- **Prefix** - текст перед значением в виджете **Label**
- **Postfix** - текст после значения в виджете **Label**

### Set text value when checked

Изменение текста виджета **Label** в зависимости от отмеченного или неотмеченного состояния целевого объекта

- **Target** - виджет **Label**, в котором отображается значение
- **Source** - состояние целевого виджета
- **On text** - текст, когда значение состояния "Отмечен"
- **Of text** - текст, когда значение состояния "Не отмечен"

### Delete screen

Удаление выбранного экрана

- **Screen** - экран, который необходимо удалить

### Step spinbox

Увеличение или уменьшение значения виджета **Spinbox**

- **Target** - целевой виджет **Spinbox**
- **Direction** - направление значения (увеличение или уменьшение счетчика)
