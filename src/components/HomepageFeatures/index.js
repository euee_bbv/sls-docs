import clsx from "clsx";
import Heading from "@theme/Heading";
import styles from "./styles.module.css";

const FeatureList = [
  {
    title: "Все в одном",
    // Svg: require("@site/static/img/undraw_docusaurus_mountain.svg").default,
    description: (
      <>
        Проектируйте, прототипируйте и разрабатывайте в одном и том же
        программном обеспечении
      </>
    ),
  },

  {
    title: "LIVE Превью",
    // Svg: require("@site/static/img/undraw_docusaurus_react.svg").default,
    description: (
      <>
        Просто нажмите кнопку «Воспроизвести», чтобы мгновенно опробовать
        пользовательский интерфейс, не перестраивая его в режиме
        предварительного просмотра с точностью до пикселя
      </>
    ),
  },
  {
    title: "Независимость от поставщика",
    // Svg: require("@site/static/img/undraw_docusaurus_react.svg").default,
    description: (
      <>
        Экспортирует независимый от платформы код C или MicroPython для LVGL,
        который можно скомпилировать для устройства любого производителя
      </>
    ),
  },
];

function Feature({ title, description }) {
  return (
    <div className={clsx("col col--4")}>
      <div className="text--center"></div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
